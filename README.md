WARNING:

This will enable and disable the On-Screen Keyboard provided with Windows XP Tablet Edition on the Windows login prompt. Use with caution or you might find yourself unable to use the touchscreen to log in!